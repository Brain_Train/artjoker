import React, { useState } from 'react'

const Task = () => {
   const [value, setValue] = useState([]);
   const [number, setNumber] = useState(0);
   const [bonus, setBonus] = useState(0);

   let currentValue = number.toString()
   for (let i = 0; i < currentValue.length; i++) {
      value.push(+currentValue.charAt(i));
   }

   const highPrise = (num) => {
      let oddNumbers = [];
      let allOdds = [];
      for (let i = 0; i < currentValue.length; i++) {
         if (num[i] % 2) {
            oddNumbers.push(num[i]);
         } else {
            oddNumbers.push(undefined);
         }
      }
      for (let i = 0; i < oddNumbers.length; i++) {
         if (oddNumbers[i] % 2 && oddNumbers[i + 1] % 2 && oddNumbers[i + 2] === undefined) {
            allOdds.push(oddNumbers[i], oddNumbers[i + 1]);
         }
         else if (oddNumbers[i] % 2 && oddNumbers[i + 1] % 2 && oddNumbers[i + 2] % 2) {
            break;
         }
      }
      if (allOdds[0] < allOdds[1] && allOdds[2] < allOdds[3]) {
         setBonus(2000);
      }
      else {
         mediumPrise(num);
      }
      console.log('oddNumbers', oddNumbers);
      console.log('allOdds', allOdds);
   }
   const mediumPrise = (num) => {
      let oddNumbers = [];
      let allOdds = [];
      for (let i = 0; i < currentValue.length; i++) {
         if (num[i] % 2) {
            oddNumbers.push(num[i]);
         } else {
            oddNumbers.push('');
         }
      }
      for (let i = 0; i < oddNumbers.length; i++) {
         if (oddNumbers[i] % 2 && oddNumbers[i + 1] % 2 && oddNumbers[i + 2] === '') {
            allOdds += 1;
         } else if (oddNumbers[i] % 2 && oddNumbers[i + 1] % 2 && oddNumbers[i + 2] % 2) {
            break;
         }
      }
      if (allOdds >= 2) {
         setBonus(1000);
      } else {
         lowPrise(num);
      }
   }
   const lowPrise = (num) => {
      let evenNumbers = [];
      let oddNumbers = [];
      for (let i = 0; i < currentValue.length; i++) {
         if (num[i] !== undefined) {
            if (num[i] % 2) {
               oddNumbers += num[i];
            }
            else {
               evenNumbers += num[i];
            }
         }
      }
      if (evenNumbers > oddNumbers) {
         setBonus(100);
      } else {
         setBonus(0);
      }
      console.log('even', evenNumbers);
   }

   return (
      <div>
         <h2>Bonus: {bonus} UAH</h2>
         <input type="text" maxLength="8" onChange={(e) => setNumber(e.target.value)} />
         <button type='submit' onClick={() => { highPrise(number) }}>Send promo</button>
         <div>
            <h5>97546128 - promo is not valid</h5>
            <h5>48183276 - 100 UAH</h5>
            <h5>73289388 - 1000 UAH</h5>
            <h5>37283988 - 2000 UAH</h5>
         </div>
      </div>
   )
}

export default Task
