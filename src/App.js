import './App.css';

import Task from './components/task';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Task/>
      </header>
    </div>
  );
}

export default App;

